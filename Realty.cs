﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstate
{
    // public pour être accessible partout, abstract car il n y a pas intérêt de créer des objets de type bien en temps que tel
    public abstract class Realty
    {
        protected int registerNumber;
        protected string location;
        protected int area;
        protected decimal rent;

        public int RegisterNumber { get => registerNumber; }

        public Realty(int _resgisterNumber, string _location, int _area, decimal _rent)
        {
            registerNumber = _resgisterNumber;
            location = _location;
            area = _area;
            rent = _rent;
        }
        public override string ToString()
        {
            return "Bien locatif n° : " + registerNumber + "\nAdresse : " + location + ".\nSuperficie : " + area + "m2.\nLoyer : " + rent + " par mois.";
        }
    }
}
