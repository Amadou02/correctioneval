﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstate
{
    public class House : Realty
    {
        private int rooms;
        /// <summary>
        /// Hydrate l'objet avec la valeur passées en argument lors de son instanciation
        /// </summary>
        /// <param name="_location">Adresse du bien</param>
        /// <param name="_area">Superficie du bien</param>
        /// <param name="_rent">Loyer du bien</param>
        public House(int _registerNumber,string _location,int _area, decimal _rent, int _rooms):base(_registerNumber, _location, _area, _rent)
        {
            rooms = _rooms;
        }
        /// <summary>
        /// Permet d'adapter l'affiche pour l'objet house
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return base.ToString() + "\nNombre de pièce : " + rooms;
        }
    }
}
