﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RealEstate
{
    class Program
    {
        static List<Realty> realties;
        // Debut de la méthode principale
        static void Main(string[] args)
        {
            // Initialisation de la liste de bien
            realties = new List<Realty>();
            // Création d'un menu permettant à l'utilisateur de choisir une opération
            // 1 = Lister les biens; 2 = Ajouter un biens; 3 = Supprimer un bien; 4 = nombre de bien dans le portefeuille; 5 quitter l'application

            // On affiche le menu pour recupérer le choix de l'utilisateur
            int choice = DisplayMenu();
            // Choix qui permet d'arrêter l'application
            while (choice != 6)
            {
                switch (choice)
                {
                    case 1:
                        ListRealies();
                        break;
                    case 2:
                        AddFlat();
                        break;
                    case 3:
                        AddHome();
                        break;
                    case 4:
                        AddParking();
                        break;
                    case 5:
                        DeleteRealty();
                        break;
                }
                choice = DisplayMenu();
            }
        }

        ////////////////////////// Partie ajout de biens ///////////////////////////////////////

        private static void AddFlat()
        {
            // On nettoie la console.
            Console.Clear();
            // Le labelle de l'ajout d'appartement
            Console.WriteLine("Ajout d'un appartement");
            // Un saut de ligne pour la lisibité
            Console.Write("\n");
            // On récupère les données de l'appartement
            // On contrôle la saisie utilisateur avant de l'accepter
            Console.Write("Numero d'enrégistrement : ");
            int registerNumber = getInteger();
            Console.Write("Localisation du bien : ");
            String location = Console.ReadLine();
            // On contrôle la saisie utilisateur avant de l'accepter
            Console.Write("Superficie : ");
            int area = getInteger();
            // On contrôle la saisie utilisateur avant de l'accepter
            Console.Write("Loyer : ");
            decimal rent = getDecimal();
            // On contrôle la saisie utilisateur avant de l'accepter
            Console.Write("Nombre de pièce : ");
            int rooms = getInteger();
            Console.Write("Etage : ");
            int floor = getInteger();
            // On instancie un nouvelle objet flat avec les données renseignées.
            Flat flat = new Flat(registerNumber, location, area, rent, rooms, floor);
            // On ajoute le nouvel appartement à la liste des biens
            realties.Add(flat);
            // Un saut de ligne pour la lisibilité
            Console.Write("\n");

        }

        private static void AddParking()
        {
            // On nettoie la console.
            Console.Clear();
            // Le labelle de l'ajout d'appartement
            Console.WriteLine("Ajout d'un parking");
            // Un saut de ligne pour la lisibité
            Console.Write("\n");
            // On récupère les données de l'appartement
            // On contrôle la saisie utilisateur avant de l'accepter
            Console.Write("Numero d'enrégistrement : ");
            int registerNumber = getInteger();
            Console.Write("Localisation du bien : ");
            String location = Console.ReadLine();
            // On contrôle la saisie utilisateur avant de l'accepter
            Console.Write("Superficie : ");
            int area = getInteger();
            // On contrôle la saisie utilisateur avant de l'accepter
            Console.Write("Loyer : ");
            decimal rent = getDecimal();
            // On instancie un nouvelle objet parking avec les données renseignées.
            Parking parking = new Parking(registerNumber, location, area, rent);
            // On ajoute le nouvel appartement à la liste des biens
            realties.Add(parking);
            // Un saut de ligne pour la lisibilité
            Console.Write("\n");
        }

        private static void AddHome()
        {
            // On nettoie la console.
            Console.Clear();
            // Le labelle de l'ajout d'appartement
            Console.WriteLine("Ajout d'une maison");
            // Un saut de ligne pour la lisibité
            Console.Write("\n");
            // On récupère les données de l'appartement
            // On contrôle la saisie utilisateur avant de l'accepter
            Console.Write("Numero d'enrégistrement : ");
            int registerNumber = getInteger();
            Console.Write("Localisation du bien : ");
            String location = Console.ReadLine();
            // On contrôle la saisie utilisateur avant de l'accepter
            Console.Write("Superficie : ");
            int area = getInteger();
            // On contrôle la saisie utilisateur avant de l'accepter
            Console.Write("Loyer : ");
            decimal rent = getDecimal();
            // On contrôle la saisie utilisateur avant de l'accepter
            Console.Write("Nombre de pièce : ");
            int rooms = getInteger();
            // On instancie un nouvelle objet flat avec les données renseignées.
            House house = new House(registerNumber, location, area, rent, rooms);
            // On ajoute le nouvel appartement à la liste des biens
            realties.Add(house);
            // Un saut de ligne pour la lisibilité
            Console.Write("\n");
        }
        ///////////////////// Suppression de biens /////////////////////////////////
        private static void DeleteRealty()
        {
            // Saut de ligne pour la lisibilité
            Console.Write("\n");
            // Libellé pour le choix du bien à supprimer
            Console.WriteLine("Suppression d'un bien du parc immobilier");
            Console.Write("Numéro du bien que vous souhaitez-vous supprimer ?");
            int numRealty = getInteger();
            // On cherche l'index du bien à supprimer s'il existe dans la liste
            int index = FindRealty(numRealty);
            // On le bien existe, on le supprime et on avise l'utilisateur
            if(index >= 0)
            {
                realties.RemoveAt(index);
                Console.WriteLine("Le bien a été supprimé avec succès !\n");
            }
            else
            {
                Console.WriteLine("Le bien que vous voulez supprimer n'existe pas !");
            }
            // Saut de ligne pour la lisibilité
            Console.Write("\n");
        }
        /// <summary>
        /// Recherche l'index dans la liste d'un bien à partir de son numéro d'enrégistrement
        /// </summary>
        /// <param name="numRealty"></param>
        /// <returns> L'index </returns>
        private static int FindRealty(int numRealty)
        {
            // On initialise avec un index qui ne peut pas exister dans la liste
            int index = -1;
            // On parcours la liste des biens et on compare chaque numero avec le numéro fourni
            foreach(Realty realty in realties)
            {
                // L'attribut est emcapsulé dans un propriété qui permet son accès en lecture seule
                if (realty.RegisterNumber == numRealty)
                {
                    return realties.IndexOf(realty);
                }
            }
            return index;
        }

        
        /// <summary>
        /// Permet de s'assurer de la saisie d'un nombre décimal
        /// </summary>
        /// <returns>Nombre décimal</returns>
        private static decimal getDecimal()
        {
            while (true)
            {
                decimal number;
                if (decimal.TryParse(Console.ReadLine(), out number))
                {
                    return number;
                }
            }
        }

        /// <summary>
        /// Permet de s'assurer de la saisie d'un nombre entier
        /// </summary>
        /// <returns>Retourne un entier</returns>
        private static int getInteger()
        {
            while (true)
            {
                int number;
                if(int.TryParse(Console.ReadLine(), out number))
                {
                    return number;
                }
            }
        }

        /// <summary>
        /// Affiche le menu et récupère le choix utilisateur
        /// </summary>
        /// <returns>Le choix utilisateur</returns>
        private static int DisplayMenu()
        {
            // On affiche le menu jusqu'à l'obtention d'un choix correct
            // On créer une boucle infinie dont la sortie sera le return de la fonction
            while (true)
            {
                Console.WriteLine("gestion du parc immobilier de l'agence");
                Console.WriteLine("Le stock immobilier de l'agence est, actuellement, de : " + realties.Count + " biens.");
                // Un saut de ligne pour la lisibilité
                Console.Write("\n");
                // Choix possibles
                Console.WriteLine("Menu de l'application\n");
                Console.WriteLine("1 : Afficher la liste des biens");
                Console.WriteLine("2 : Ajouter un appartement");
                Console.WriteLine("3 : Ajouter une maison");
                Console.WriteLine("4 : Ajouter un parking");
                Console.WriteLine("5 : Supprimer un bien");
                Console.WriteLine("6 : Quitter l'application");

                // Reéupère la saisie utilisateur pour la stockée dans la variable response.
                String response = Console.ReadLine();
                // On déclare une variable local choice de type entier et on l'affecte le résultat de la conversion.
                int choice;
                // On essaye la conversion.
                if (int.TryParse(response, out choice))
                {
                    // La saisie est un entier : on verifie qu'elle correspond à l'un des choix attendus
                    if (choice > 0 && choice <= 6)
                    {
                        return choice;
                    }

                }
                // On nettoie la console après chaque échec avant de réitérer la demande.
                Console.Clear();
            }
        }

        
        private static void ListRealies()
        {
            // On nettoie la console.
            Console.Clear();
            // Labelle de la liste
            Console.WriteLine("Liste des biens : \n");
            // On boucle la liste pour afficher chaque objet bien
            foreach(Realty real in realties)
            {
                // Grâce à notre méhtode ToString, nous pouvons afficher sous la forme d'une chaine notre objet.
                Console.WriteLine(real);
                // Un saut de ligne pour la lisibité
                Console.Write("\n");
            }
            // Un saut de ligne pour la lisibité
            Console.Write("\n");
        }
    }
}
